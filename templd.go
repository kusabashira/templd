package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"runtime"
)

var (
	COMMAND_NAME       = "templd"
	VERSION            = "v0.2.0"
	USERS_TEMPLATE_DIR = ".templd"
)

func showUsage() {
	fmt.Fprintf(os.Stderr, `
Usage: %s [OPTION] NAME
Create entity of template directory

Options:
	--template -t   template name(need!)
	--help          show this help message
	--version       print the version

Template directory is "~/.templd".
You can put into template directories.
directory name will be template name.

Report bugs to <kusabashira227@gmail.com>
`[1:], COMMAND_NAME)
}

func showVersion() {
	fmt.Fprintln(os.Stderr, "%s: %s\n",
		COMMAND_NAME, VERSION)
}

var (
	AfterProcessFile string
	GetCopyCommand   func(templatePath, entityPath string) *exec.Cmd
)

func init() {
	if runtime.GOOS == "windows" {
		AfterProcessFile = "templd.bat"
		GetCopyCommand =
			func(templatePath, entityPath string) *exec.Cmd {
				return exec.Command("cmd", "/c", "xcopy",
					templatePath, entityPath,
					"/S", "/R", "/Y", "/I", "/K")
			}
	} else {
		AfterProcessFile = ".templd"
		GetCopyCommand =
			func(templatePath, entityPath string) *exec.Cmd {
				return exec.Command("cp", "-R",
					templatePath, entityPath)
			}
	}
}

func homeDirectory() (string, error) {
	user, err := user.Current()
	if err != nil {
		return "", err
	}
	return user.HomeDir, nil
}

func checkWhetherDirectory(path string) error {
	finfo, err := os.Stat(path)
	if err != nil {
		return err
	}
	if finfo.IsDir() {
		return nil
	}
	return fmt.Errorf("%s is not directory", path)
}

func createUsersTemplateDirectory(usersTemplatePath string) error {
	err := os.Mkdir(usersTemplatePath, 0755)
	if err != nil {
		return err
	}
	return fmt.Errorf("%s was not exist, and created %s",
		usersTemplatePath, usersTemplatePath)
}

func isExistAfterProcessFile(templatePath string) (bool, error) {
	afterProcessFilePath, err :=
		filepath.Abs(filepath.Join(templatePath, AfterProcessFile))
	if err != nil {
		return false, err
	}
	_, err = os.Stat(afterProcessFilePath)
	switch {
	case os.IsNotExist(err):
		return false, nil
	case err != nil:
		return false, err
	default:
		return true, nil
	}
}

func afterProcess(entityPath string, args []string) error {
	err := os.Chdir(entityPath)
	if err != nil {
		return err
	}
	cmd := exec.Command(AfterProcessFile, args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Start()
	if err != nil {
		return err
	}
	err = cmd.Wait()
	if err != nil {
		return err
	}
	return os.Remove(AfterProcessFile)
}

func _main() error {
	var templateName string
	flag.StringVar(&templateName, "t", "", "template directory name")
	flag.StringVar(&templateName, "template", "", "template directory name")

	var isHelp, isVersion bool
	flag.BoolVar(&isHelp, "help", false, "show this help message")
	flag.BoolVar(&isVersion, "version", false, "print the version")

	flag.Usage = showUsage
	flag.Parse()
	if isHelp {
		showUsage()
		return nil
	}
	if isVersion {
		showVersion()
		return nil
	}

	homePath, err := homeDirectory()
	if err != nil {
		return err
	}
	usersTemplatePath := filepath.Join(homePath, USERS_TEMPLATE_DIR)
	err = checkWhetherDirectory(usersTemplatePath)
	if os.IsNotExist(err) {
		return createUsersTemplateDirectory(usersTemplatePath)
	}
	if err != nil {
		return err
	}

	var templatePath string
	if templateName == "" {
		return fmt.Errorf("no specify template name")
	}
	templatePath = filepath.Join(usersTemplatePath, templateName)
	err = checkWhetherDirectory(templatePath)
	if err != nil {
		return err
	}

	var entityPath string
	if flag.NArg() < 1 {
		return fmt.Errorf("no specify entity name")
	}
	entityPath = flag.Arg(0)

	err = GetCopyCommand(templatePath, entityPath).Run()
	if err != nil {
		return err
	}

	isExistAfterProcess, err := isExistAfterProcessFile(templatePath)
	if err != nil {
		return err
	}
	if isExistAfterProcess {
		err = afterProcess(entityPath, flag.Args())
		if err != nil {
			return err
		}
	}

	return nil
}

func main() {
	err := _main()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n",
			COMMAND_NAME, err)
		os.Exit(1)
	}
}
